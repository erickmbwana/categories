import json

from rest_framework import status
from rest_framework.test import APITestCase

from .models import Category, Product


class CategoryViewSetTestCase(APITestCase):

    def test_post(self):
        data = {'name': 'Clothing'}
        res = self.client.post('/api/categories/', data=data)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Category.objects.count(), 1)

    def test_delete(self):
        data = {'name': 'Clothing'}
        self.client.post('/api/categories/', data=data)

        self.assertEqual(Category.objects.count(), 1)

        # Do a DELETE request
        res = self.client.delete('/api/categories/1/')

        self.assertEqual(Category.objects.count(), 0)
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)

    def test_get(self):
        # bulk Create
        data = [
            {"name": 'Phones', "description": "Best phones ever"},
            {"name": 'Books', "description": "Quality affordable books"},
            {"name": 'Clothing', "description": "The latest styles and fashions"}
        ]
        categories = [Category(**kwargs) for kwargs in data]
        Category.objects.bulk_create(categories)

        expected = [
            {
                "id": 1,
                "name": "Phones",
                "description": "Best phones ever",
            },
            {
                "id": 2,
                "name": "Books",
                "description": "Quality affordable books",
            },
            {
                "id": 3,
                "name": "Clothing",
                "description": "The latest styles and fashions",
            }
        ]
        res = self.client.get('/api/categories/')

        actual = json.dumps(res.data, sort_keys=True)
        self.assertJSONEqual(actual, expected)


class ProductViewSetTestCase(APITestCase):

    def setUp(self):
        super().setUp()
        Category.objects.create(name='Phones')

    def test_post(self):
        data = {'name': 'Samsung J2', 'category': 1}
        res = self.client.post('/api/products/', data=data)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Product.objects.count(), 1)

    def test_delete(self):
        data = {'name': 'Samsung J2', 'category': 1}
        self.client.post('/api/products/', data=data)

        self.assertEqual(Product.objects.count(), 1)
        # Do a DELETE request

        res = self.client.delete('/api/products/1/')

        self.assertEqual(Product.objects.count(), 0)
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)

    def test_get(self):
        # bulk Create
        names = ['Redmi 4X', 'HTC 830', 'Tecno Spark']
        cat = Category.objects.first()
        products = [Product(name=n, category=cat) for n in names]
        Product.objects.bulk_create(products)

        expected = [
            {
                "id": 1,
                "name": "Redmi 4X",
                "description": "",
                "category": 1
            },
            {
                "id": 2,
                "name": "HTC 830",
                "description": "",
                "category": 1
            },
            {
                "id": 3,
                "name": "Tecno Spark",
                "description": "",
                "category": 1
            }
        ]
        res = self.client.get('/api/products/')

        actual = json.dumps(res.data, sort_keys=True)
        self.assertJSONEqual(actual, expected)
