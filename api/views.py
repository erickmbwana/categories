from rest_framework import viewsets


from .models import Category, Product
from .serializers import CategorySerializer, ProductSerializer


class CategoryViewSet(viewsets.ModelViewSet):

    """
    API endpoint that allows categories to be viewed, edited or deleted.
    """

    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class ProductViewSet(viewsets.ModelViewSet):

    """
    API endpoint that allows products to be viewed, edited or deleted.
    """

    queryset = Product.objects.all()
    serializer_class = ProductSerializer
