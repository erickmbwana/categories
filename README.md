### Introduction

This is a Django REST application which is used to manage the following resources:
1. `Category` - a product category which has a name and a description.
2. `Product` - a product which can belong to any category.  It can also have a name
and a description.

Descriptions are optional in both resources.

The application exposes the following REST endpoints(no authentication is required):

|                     | GET                     | POST                | PUT                       | DELETE                    |
|---------------------|-------------------------|---------------------|---------------------------|---------------------------|
| api/categories/     | Show all categories     | Add a new  category | N/A                       | Delete all categories     |
| api/categories/`id` | show category with `id` | N/A                 | Update category with `id` | Delete category with `id` |
| api/products/       | Show all products       | Add a new product   | N/A                       | Delete all products       |
| api/products/`id`   | Show product with `id`  | N/A                 | Update product with `id`  | Delete product with `id`  |


### Installation

In a separate Python virtual environment running on Python 3.5 or above, do:

`pip install -r requirements.txt`

Access the endpoints using `curl` or any other client of your choice.

### Tests

To run the tests do:

`python manage.py test`
